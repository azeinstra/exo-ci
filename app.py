from flask import Flask
from calc import square


app = Flask(__name__)

@app.route("/")
def home():
    return "ok"

@app.route("/square/<number>")
def squareCalc(number):
    result=None
    try:
        number=float(number)
    except:
        return "Problème. . ."
    try:
        result=square(number)
    except:
        return "Problème. . ."
    return f'Le carré de {number} est {result}.'

app.run(host="0.0.0.0", port=8081)