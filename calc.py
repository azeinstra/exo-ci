from typing import Type


def square(n):
    if type(n) not in [int, float]:
         raise TypeError("Le nombre doit être réel.")
    return n*n

print(square(4))