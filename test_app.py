import unittest
from calc import square


class TestSquareCalc(unittest.TestCase):
    def test1_squareCalc(self):
        self.assertEqual(square(1), 1)
        self.assertEqual(square(2), 4)
        self.assertEqual(square(3), 9)
    
    def test_types(self):
        self.assertRaises(TypeError, square, True)
        self.assertRaises(TypeError,square, "5.2")

